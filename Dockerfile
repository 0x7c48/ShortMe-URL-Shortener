FROM python:3

WORKDIR /app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5555

# dev mode CMD ["python", "run.py"]

# prod mode
CMD gunicorn -w 4 -b 0.0.0.0:5555 run:app
